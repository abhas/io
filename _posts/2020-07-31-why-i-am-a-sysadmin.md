---
title: Why I am a sysadmin
date: 2020-07-31
Author: Abhas Abhinav
layout: post
permalink: /sysadmin/
toc: true
image: /images/sysadmin.png
subtitle:
  I consider myself to be a sysadmin. More than a "job description", its 
  an attitude and value system. And today, on SysAdmin Day, as a tribute to
  sysadmins and hackers everywhere, I thought I must share my perspective about
  why I am a sysadmin.
description:
  I consider myself to be a sysadmin. More than a "job description", its
  an attitude and value system. And today, on SysAdmin Day, as a tribute to
  sysadmins and hackers everywhere, I thought I must share my perspective about
  why I am a sysadmin.

---

![Why I am a Sysadmin](/images/sysadmin.png)

Foremost - I consider myself to be a sysadmin. More than a "job
description", its an attitude and value system. And today, on SysAdmin Day, as
a tribute to sysadmins and hackers everywhere, I thought I must share my
perspective about why I am a sysadmin.

## Sysadmin first, Programmer next

Why do you program? To solve a problem. Who defines the problem to you?

Being a sysadmin is a process of discovering new problems in new
situations. Sometimes, you can solve those problems with quick hacks and
creativity. When you can't, you write a program.

Thinking like (and as) a sysadmin enables me to always first assess
existing tools, methods and options. Then when you write code it is to
fill in a gap or extend or fix an existing system.

For me, thinking as a sysadmin is a way of generating long-term
programming projects. It also enables me to choose more complex
programming projects that depend on servers or services or
infrastructure being in a specific state.

## An opportunity to know things

Often focussing just on programming does not give us sufficient exposure
to how things work "under the hood". A program can also often depend on
a variety of services and understanding how those services work and how
they interact with each other can be integral to how the program is
developed.

But most importantly, intricate knowledge of how things happen at the
operating system, network, hardware or service is a very important
skill. These insights can help in designing more efficient systems and
programs.

## Laziness is a virtue!

As a sysadmin (and a programmer) I am lazy. If I set something up and
its working, I rarely want to make changes to it. The same holds true
for a program as well.

However, the reason such laziness is a virtue is that if you know that
you are going to be lazy in future, you can do two things pro-actively:

  - You can build a process that is not costly (in terms of effort) and
    which encourages rapid change

  - You can foresee issues that might come up and compensate for them
    in advance


## Being a sysadmin has taught me a lot about responsibility, accountability and collaboration

When I build and operate and maintain services that others use, there
is a lot of responsibility that lies on me. I need to my work
responsibly (can't do a bad job that doesn't offer the value a user is
looking for), carefully (ie. I can't be careless) and quickly (who wants to
wait an inordinate amount of time waiting for a fix).

I should also hold myself accountable if (or when) I make a mistake that
affects users. I should also build things in a manner that they are easy
to monitor and maintain by others should I be unavailable.

These are all things I repeatedly learn as I perform my sysadmin roles
at home, at my workplace and for our customers alike. I don't think
there is a better way to learn these values and practice them on a daily
basis.

## "Being There" takes a new meaning

Its often said that being a sysadmin is a thankless job. People are
quick to point out when you mess up and don't want to leave you alone
till you've solved their problem.

Hence, "being there" and available takes a new meaning. There will
always be situations where I might need to do things when there are
other conflicting priorities to deal with.

## The delight of a job well done!

What is the experience of discovering a system that's been running for 4
years without requiring a reboot, login or maintenance? ... which you
didn't even know was still online and working and providing a service to
its users?

The sheer delight of making a discovery of this sort is beyond compare.
It makes the whole process of administering systems so worth it

## Learning, taking risks and building confidence

Doing things, getting my hands "dirty" and seeing them work out is great
learning. Doing such thing repeatedly builds confidence and that gives
me the ability to take risks, make quick decisions and build bold
cross-connections.

All these things are integral to choosing to be a good sysadmin.

## I can type fast! :)

It would be painful to have a command in your head and then be limited
by your ability to only type so fast to see what would come out of
executing it.

I learnt to touch-type because I took up typing in school. And much
later on, it only helped me become very productive as a sysadmin. More
so with hackable mechanical keyboards.

## Hardware development as a sysadmin

I like to think that it is possible to visualise any problem in a
"sysadmin manner". I imagine hardware development that way. Knowledge of
how to connect systems together can help build hardware that works and
connects in creative ways.

*(How about running a web-server on a micro-controller so that Prometheus
can scrape its sensors' values and show them to you on a Grafana
dashboard?)*

## Thinking in pipes

I often think in "pipes". Pipes are an excellent way to do quick hacks
on the command-line. And if you can extend that value system a bit
further, you can connect things in disparate ways without having to
over-engineer them or re-invent the wheel.

## Getting the computer to work for you...

... as compared to us doing something ourselves. Often, our methods of
choosing to do things are not optimized for using the power of a
computer to our advantage. We do too much manual labour because
automating something on a computer is tedious job.

One thing being a sysadmin has taught me is about the power of
automation. If I can get the computer to do something and do it again
and again, it definitely isn't going to get fed-up or tired. On the
other hand, unless I did that I would be using my own time in the most
sub-optimal manner.

Understanding an operating system or a network or a system well enough
to use all of its power in our favour is an excellent sysadmin skill.
And I am fortunate to have had the opportunity to examine this value the
hard way -- by making mistakes. Very tedious ones at times!

## My powerpack: GNU + Debian + vim + SSH + Bash + Ansible + Docker

I don't like monolithic systems because they go against the "unix"
philosophy of one little tool doing one thing well. (I'm talking about
systemd here, of course!)

A combination of GNU utilities, Debian, vim, ssh and bash form the most
important tools that I can ever need to get work done. And now ansible
and docker complete the power-pack to provide the elements I'd been
missing. I believe that knowing just these tools well is sufficient to
handle any sysadmin task effectively. Just these.

## Its a practice - it needs practice

["The Practice of System and Network Administration"](https://the-sysadmin-book.com/) is an iconic book
that I discovered quite some time back. When I discovered it and read
it, I realised that it contained every bit of information and set of
practices that a sysadmin would ever need. Not just technical skills,
but the book could also teach us so many non-technical skills that are
essential in our sysadmin lives.

Among most bits of advice that stood out, three things stood out for me:

  - the authors chose to look at sysadmin work as a practice - like what
    a doctor would do!

  - their advice on fixing each problem just once is my personal ethic
    for building automation and productising sysadmin work

  - their chapter on time-management was a gem - being to "get things
    done" and remaining productive are essential traits and
    time-management plays no small part in that

Like with all other practices (badminton or medicine, for example), the
practice of being a sysadmin also needs practice. Its not a designation
one earns solely out of getting hired or promoted or delegated. Being a
practicioner is what matters - without practical, actionable skills and
knowledge, I couldn't have been a sysadmin at all.

## With the detachment of a doctor and the empathy of a nurse...

Finally, I feel, being a sysadmin has helped me develop greater empathy
for users of any system I build. Excelling at building systems that
people like to use and which solve their problems is great fun. However,
at the same time one can not sacrifice stoic objectivity and pragmatism.

Being a sysadmin is a healthy mix of both.

## Strength of a community and aspiring to contribute back...

I can not do justice to a subject as critical as system administration
without talking about what it means to be a part of a community. I am
glad that sysadmins love to share what they build. And when sysadmins
write software to solve problems, its fascinating to learn about how to
build little bits that do a significant job.

Unless everyone could share and collaborate and help each other, it
would be impossible to learn. I learn not just from forum posts where
someone might have got a solution for exactly the same error I'm facing.
When sysadmins write documentation or books, create automation tools,
package software or get it to inter-operate, there are many more ways to
learn from.

